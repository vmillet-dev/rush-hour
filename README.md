# Rush Hour Solveur 

## Installation

### Linux

#### Dépendances

* cmake (version min 3.8.0)
* make
* g++
* gcc

#### Build

```
git clone https://gitlab.com/vmillet-dev/rush-hour.git
cd rush-hour/
cmake CMakeLists.txt
make
```

#### Utilisation

```
./rush-hour [CHEMIN_DU_FICHIER]
```

Exemple pour charger un puzzle à partir d'un fichier:

```
./rush-hour Sujet/puzzle.txt
```

Exemple pour générer un puzzle:

```
./rush-hour
```

### Windows

Le build est similaire à Linux si ce n'est que des bugs ont été relévé 
sur l'affichage dans le CMD et le bash de windows
(Non prise en compte de l'UTF8) 

Il est préférable d'utiliser la console "Linux" intégrée à Windows pour cela.

# Projet Rush Hour

Le but de ce projet universitaire était d'écrire un programme permettant de 
trouver une solution au jeu *Rush Hour*. À partir d'une grille donnée en entrée
matérialisant les véhicules, le programme la solution sous la forme de
l'ensemble des étapes à effectuer pour finir le puzzle.

## Règles du jeu

Le jeu Rush Hour se joue seul sur une grille carrée de six cases de côté. Sur
cette grille sont répartis des véhicules d'une case de largeur, et de deux ou
trois cases de longueur. Ces véhicules peuvent être placés horizontalement ou
verticalement. Chaque véhicule peut être déplacé en avant ou en arrière, mais
pas latéralement, tant qu'il n'entre pas en collision avec un autre véhicule.
Le but du jeu est de faire sortir l'un des véhicules par une sortie placée sur
le bord du plateau. L'image ci dessous illustre un exemple de partie.

![Exemple Rush Hour](Sujet/rush_hour.gif)

Chaque déplacement de véhicule compte pour un coup, quelle que soit la longueur
du déplacement. La qualité de la solution dépend donc du nombre de coups
nécessaires depuis la situation initiale pour faire sortir le véhicule.

## Modélisation

Pour rentrer dans une partie plus technique, la recherche d'une solution au jeu 
Rush Hour peut être modélisée sous la forme d'un parcours de graphe. Dans 
ce graphe, les sommets sont des situations de jeu.
Les arêtes sont des coups. Les deux images qui suivent représentent deux
situations de jeu, et donc deux sommets du graphe. Il est possible de passer
d'une situation à l'autre en déplaçant le long véhicule du haut, elles sont donc
reliées par une arête dans le graphe.

![Situation depart](Sujet/rush_hour_situation_start.png)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
![Situation fin](Sujet/rush_hour_situation_end.png)

Notre première tâche pour ce projet consista à élaborer une structure de données
pour représenter les situations de jeu, munies de méthodes pour accéder de
façon pratique aux situations de jeu adjacentes.

Il ne s'agit pas ici de *construire le graphe complet* ni de le *stocker*, mais
simplement de pouvoir le *parcourir*.

Pour nous aider dans l'élaboration de notre structure de données, vous utilisa
le fait que :

* les véhicules ne sont que de taille deux ou trois
* il n'y a jamais plus de 16 véhicules
* il n'y a toujours qu'un véhicule à sortir

La situation initiale du problème résolu plus haut :

![Situation initiale](Sujet/rush_hour_initial.png)

pourra être décrite par [le fichier suivant](Sujet/puzzle.txt) :

```
2 5
2 0 2 1
0 0 2 0
0 2 3 0
0 3 3 1
1 3 2 0
1 4 2 1
2 5 2 0
3 0 2 1
4 0 2 0
4 3 2 0
4 4 2 0
4 5 2 0
5 1 2 1
```
La première ligne correspond à la position de la sortie (ligne 2 colonne 5), la
seconde ligne est la position du véhicule à sortir (ligne 2, colonne 0, longueur
2, horizontal), les lignes suivantes sont les autres véhicules, toujours avec le
format ligne, colonne, longueur, horizontal (1) ou vertical (0). Dans le cas
d'un véhicule horizontale, la position donnée est celle de la case la plus à
gauche, dans le cas d'un véhicule vertical, la position donnée est celle de la
case la plus haute.

## Parcours

Une fois les situations de jeu représentables, il s'agit maintenant d'instancier
la situation de jeu initiale, et de parcourir le graphe pour trouver une
situation de jeu gagnante, ainsi que les coups permettant de l'atteindre.
Idéalement, le nombre de coups à jouer pour atteindre cette situation de jeu
gagnante devra être minimal.

#### by Valentin MILLET et Florent CLEMENT