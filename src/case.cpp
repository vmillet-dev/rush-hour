#include <case.h>

Case::Case() {
}

Case::Case(int ligne, int colonne)
          : m_ligne(ligne), m_colonne(colonne){}

Case::Case(Case const &src) {
    this->m_ligne = src.getLigne();
    this->m_colonne = src.getColonne();
}

bool Case::operator==(const Case src) const {
    return this->m_ligne == src.m_ligne && this->m_colonne == src.m_colonne;
}
