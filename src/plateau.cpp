#include <plateau.h>
#include <set>
#include <queue>
#include <algorithm>

Plateau::Plateau(int largeur, int hauteur)
                : m_largeur(largeur), m_hauteur(hauteur) {

}


void Plateau::chargerPuzzle(const char* cheminFichier) {
  lElement.clear();
  std::ifstream fichier(cheminFichier, std::ios::in);

  if(fichier) {
    int ligneSortie, colonneSortie;
    fichier >> ligneSortie >> colonneSortie;
    this->setSortie(ligneSortie,colonneSortie);
    int lig, col, longueur, horiz;
    while (fichier >> lig >> col >> longueur >> horiz){
      lElement.push_back(Element(Case(lig, col), intToDirection(horiz), longueur));
    }
    fichier.close();
    this->m_indexElementASortir = this->getIndexVehiculeASortir();
    return;
  }
  //Cette ligne ne doit pas être excutée hormis lors d'un problème de lecture
  throw std::exception();

}

Direction Plateau::intToDirection(int orientation) {
  return (orientation == 1) ? Direction::OUEST : Direction::NORD;
}

int Plateau::directionToInt(Direction dir) {
  return (dir == Direction::EST || dir == Direction::OUEST)
         ? 1 : 0;
}

void Plateau::deplacerElement(int index, Direction direction, unsigned int longueur) {
   this->lElement.at(index).deplacer(direction, longueur);
}

Element *Plateau::getElementByCase(Case src) {
    for (Element& element : this->lElement) {
        if(element.estOccupee(src))
            return &element;
    }
    return nullptr;
}

bool Plateau::deplacerEstPossible(Case src) {
  if(src.getLigne() < 0 || src.getColonne() < 0 ||
  src.getLigne() >= this->getHauteur() || src.getColonne() >= this->getLargeur())
    return false;
  return this->getElementByCase(src) == nullptr;
}

bool Plateau::plateauExiste(std::unordered_set<std::string > lPlateau, std::string stringPlateau){
  return lPlateau.find(stringPlateau) != lPlateau.end();
}

std::vector<Mouvement> Plateau::getListeDesMouvementPossible(int indexElement) {
  std::vector<Mouvement> lMouvementPossible;
  unsigned int longueur = 1;

  Element elementDir1 = Element(this->lElement.at(indexElement));
  elementDir1.deplacer(elementDir1.getDirection(),1);
  while(deplacerEstPossible(elementDir1.getFirstCase(elementDir1.getDirection()))){
    lMouvementPossible.push_back(Mouvement(
      indexElement,
      elementDir1.getDirection(),
      longueur++)
    );
    elementDir1.deplacer(elementDir1.getDirection(),1);
  }


  longueur = 1;
  Element elementDir2 = Element(this->lElement.at(indexElement));
  elementDir2.deplacer(elementDir2.getDirectionOpposee(elementDir2.getDirection()),1);
  while(deplacerEstPossible(elementDir2.getLastCase(elementDir2.getDirection()))){
    lMouvementPossible.push_back(Mouvement(
      indexElement,
      elementDir2.getDirectionOpposee(elementDir2.getDirection()),
      longueur++)
    );
    elementDir2.deplacer(elementDir2.getDirectionOpposee(elementDir2.getDirection()),1);
  }

  return lMouvementPossible;
}

int Plateau::getIndexVehiculeASortir() {
  Direction directionRecherche;

  if(this->getHauteur() == sortie.getLigne() + 1)
    directionRecherche = NORD;
  else
    directionRecherche = OUEST;

  int indexElementASortir = -1;
  Element *element, *aSortir;
  for(int index = 0; index < this->lElement.size(); index++){
    element = &this->lElement.at(index);

    if(directionRecherche == NORD){

      if( element->getDirection() == NORD &&
          element->getCase().getColonne() == this->sortie.getColonne()){

        if(indexElementASortir == -1) {
          indexElementASortir = index;
          aSortir = &this->lElement.at(indexElementASortir);
        }
        else if(abs(aSortir->getCase().getLigne() - this->sortie.getLigne()) <
                    element->getCase().getLigne() - this->sortie.getLigne()){
          indexElementASortir = index;
          aSortir = &this->lElement.at(indexElementASortir);
        }
      }
    }
    else{
      if(element->getDirection() == OUEST &&
         element->getCase().getLigne() == this->sortie.getLigne()){

        if(indexElementASortir == -1) {
          indexElementASortir = index;
          aSortir = &this->lElement.at(indexElementASortir);
        }
        else if(abs(aSortir->getCase().getColonne() - this->sortie.getColonne()) <
                element->getCase().getColonne() - this->sortie.getColonne()){
          indexElementASortir = index;
          aSortir = &this->lElement.at(indexElementASortir);
        }
      }
    }
  }
  return indexElementASortir;
}


std::vector<Mouvement> Plateau::resoudre(const Plateau src){
  std::queue<Plateau> lPlateauPossible;
  std::unordered_set<std::string > lPlateauUtilise;
  lPlateauPossible.push(src);
  lPlateauUtilise.insert(src.to_str());
  Plateau plateau = src;

  while(!lPlateauPossible.empty() && plateau.distanceSortie() != 0) {
    plateau = lPlateauPossible.front();
    lPlateauPossible.pop();
    for (int i = 0; i < plateau.lElement.size(); i++) {
      std::vector<Mouvement> lMouvementElement = plateau.getListeDesMouvementPossible(i);
      for (Mouvement mouvement: lMouvementElement) {
        Plateau clone = plateau.clonePlateauAvecMouvement(mouvement);
        if (!plateauExiste(lPlateauUtilise, clone.to_str())) {
          lPlateauPossible.push(clone);
          lPlateauUtilise.insert(clone.to_str());
        }
      }
    }
  }
  if(plateau.distanceSortie() != 0)
    return std::vector<Mouvement>();
  else
    return plateau.m_listeMouvement;
}


std::vector<Mouvement> Plateau::resoudre() {
  return this->resoudre(*this);
}

int Plateau::distanceSortie() const {
  return this->distanceSortie(this->lElement.at(this->m_indexElementASortir));
}

int Plateau::distanceSortie(Element element) const {
  if(element.getDirection() == NORD){
    if(this->sortie.getLigne() == 0)
      return abs(element.getFirstCase(NORD).getLigne() - this->sortie.getLigne());
    else
      return abs(element.getLastCase(NORD).getLigne() - this->sortie.getLigne());
  }
  else{
    if(this->sortie.getColonne() == 0)
      return abs(element.getFirstCase(OUEST).getColonne() - this->sortie.getColonne());
    else
      return abs(element.getLastCase(OUEST).getColonne() - this->sortie.getColonne());
  }
}


Element Plateau::getElementASortir() {
  return this->lElement.at(this->getIndexVehiculeASortir());
}

Plateau Plateau::clonePlateauAvecMouvement(Mouvement mouvement) {
  Plateau plateau = Plateau(*this);
  plateau.lElement.at(mouvement.m_indexElement).deplacer(mouvement.m_direction,mouvement.m_longueur);
  plateau.m_listeMouvement.push_back(mouvement);
  return plateau;
}

bool Plateau::operator==(const Plateau plateau) const {
  return this->lElement == plateau.lElement;
}

std::string Plateau::to_str() const {
  std::stringstream buffer;
  buffer << *this;
  return buffer.str();
}

/**
* Esquisse d'une génération aléatoire mais donne des résultats médiocres
*/
void Plateau::generate() {
    m_hauteur = 6; //possibilité de modidier en paramètres plus tard
    m_largeur = 6;

    if(rand() % 2 == 0) {
        // HORIZONTAL
        if(rand()% 2 == 0){
            // HAUT
            this->setSortie(0 ,(rand() % m_largeur));
            this->lElement.push_back(Element(
                    Case(m_hauteur - 2, this->sortie.getColonne()),NORD,2));
        }
        else {
            // BAS
            this->setSortie(m_hauteur - 1,(rand() % m_largeur));
            this->lElement.push_back(Element(
                    Case(0,this->sortie.getColonne()),NORD,2));
        }

    }
    else {
        // VERTICAL
        if(rand()% 2 == 0){
            // GAUCHE
            this->setSortie((rand() % m_hauteur) ,0);
            this->lElement.push_back(Element(
                    Case(this->sortie.getLigne(), m_largeur - 2),OUEST,2));
        }
        else {
            // DROITE
            this->setSortie((rand() % m_hauteur),m_largeur - 1);
            this->lElement.push_back(Element(
                    Case(this->sortie.getLigne(), 0),OUEST,2));
        }
    }
    this->m_indexElementASortir = 0;

    int nbElement = 1;
    while(nbElement != 13 && Plateau::resoudre(*this).size() != 0) {
      Case tampon;
      Element e;
      do {
        tampon = Case(rand() % (m_largeur - 1), rand() % (m_hauteur - 1));
        if(this->lElement.at(0).getDirection() == NORD) {
            if(tampon.getLigne() == this->lElement.at(0).getCase().getLigne())
              e = Element(tampon, OUEST , 2);
            else
              e = Element(tampon, intToDirection(rand() % 2) , 2);
        }
        else {
            if(tampon.getColonne() == this->lElement.at(0).getCase().getColonne())
              e = Element(tampon, NORD , 2);
            else
              e = Element(tampon, intToDirection(rand() % 2) , 2);
        }
      } while(!estInserable(e));
      this->lElement.push_back(e);
      nbElement ++;
    }
    if(nbElement != 13)
      this->lElement.pop_back();
}

void Plateau::setSortie(int hauteur, int largeur) {
    this->sortie = Case(hauteur,largeur);
}

bool Plateau::estInserable(Element element) {
  switch (element.getDirection()) {
    case Direction::NORD:
      for(int x = element.getCase().getLigne();x < element.getCase().getLigne() + element.getLongueur();x++){
          if(this->getElementByCase(Case(x,element.getCase().getColonne())))
            return false;
      }
      break;
    case Direction::OUEST:
      for(int y = element.getCase().getColonne();y < element.getCase().getColonne() + element.getLongueur();y++){
          if(this->getElementByCase(Case(element.getCase().getLigne(), y)))
            return false;
      }
      break;
  }
  return true;
}

std::ostream& operator<<(std::ostream& os, const Plateau& p) {
  //Initialisation du tableau complet à 0 (case vide)
  int puzzle[p.getLargeur()][p.getHauteur()];
  for(int i = 0; i < p.getLargeur(); i++){
    for(int j = 0; j < p.getHauteur(); j++){
      puzzle[i][j] = 0;
    }
  }

  //Placement des éléments, chaque vehicule possède son propre numéro >0
  unsigned int taille = p.getElement().size();
  for(unsigned int i = 0; i < taille; ++i) {
    Element e = p.getElement().at(i);
     //sa place dans le tableau détermine son numéro d'où le : i+1
    if(e.getDirection() == Direction::EST || e.getDirection() == Direction::OUEST) {
      for(unsigned int curseur = 0; curseur < e.getLongueur(); ++curseur) {
        puzzle[e.getCase().getColonne()  + curseur][e.getCase().getLigne()] = i + 1;
      }
    } else {
      for(unsigned int curseur = 0; curseur < e.getLongueur(); ++curseur) {
        puzzle[e.getCase().getColonne()][e.getCase().getLigne() + curseur] = i + 1;
      }
    }
  }

  //Lecture du tableau en colorisant les véhicules, et noirçissant les cases vides
  //On utilise les caractères échappés supérieurs à 30 pour les couleurs,
  //l'addition avec la valeur du vehicule donne sa couleur, 30 étant le noir
  for(unsigned int i = 0; i < p.getHauteur(); ++i) {
      for(unsigned int j = 0; j < p.getLargeur(); ++j) {
      os << "\033[0;" << 30 + puzzle[j][i] << "m" << puzzle[j][i] << "\033[0m";
      if(puzzle[j][i] > 9) {
        os << " ";
      }else {
        os << "  ";
      }
    }
    os << std::endl;
  }
  os << std::endl;

  return os;
}
