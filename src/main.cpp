#include <plateau.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char *argv[])
{
  srand (time(NULL));
  Plateau p;
  //Test si un argument est passé, on attend le chemin ici sinon génère
  if(argc > 1) {
    //Test du chargement du fichier
    try{
        p.chargerPuzzle(argv[1]);
    }
    //Retour à l'utilisateur et fermeture du programme sinon
    catch(const std::exception&){
      std::cerr << "ERREUR : Ouverture du fichier impossible" << std::endl;
      return EXIT_FAILURE;
    }
  } else {
    std::cout << "Generation d'un puzzle en cours..." << std::endl << std::endl;
    p.generate();
  }

  std::cout << "Puzzle initial: " << "\n" << p << std::endl;

  std::cout << "Recherche de la solution en cours..." << std::endl << std::endl;

  //Stockage de la liste de mouvements permettant de trouver la solution
  std::vector<Mouvement> mouv_liste = p.resoudre();

  unsigned int i = 0;
  //Relecture de la liste des mouvements en l'affichant
  for(Mouvement mouvement : mouv_liste){
      p.deplacerElement(mouvement.m_indexElement, mouvement.m_direction, mouvement.m_longueur);
      std::cout <<
      "Coup " << ++i << " | " <<
      "Déplacement du vehicule " << mouvement.m_indexElement + 1 << "\n" <<
      p << std::endl;
  }

  std::cout << "Nombre de coups total: " << mouv_liste.size() << std::endl;

  return EXIT_SUCCESS;
}
