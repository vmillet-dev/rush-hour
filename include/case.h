#ifndef CASE_H
#define CASE_H

/*
 * Classe représentant les cases du jeu où les éléments seront positionnés
 */
class Case
{

public:

    explicit Case();

    /**
    * Construit la case via un indice de ligne et colonne
    *
    * @param ligne
    * @param colonne
    */
    Case(int ligne, int colonne);
    Case(Case const& src);

    //Getters des attributs de positionnement de la case
    const int getLigne() const { return this->m_ligne;};
    const int getColonne() const { return this->m_colonne; };

    //Setters des attributs de positionnement de la case
    void setLigne(int ligne) { this->m_ligne = ligne; }
    void setColonne(int colonne) { this->m_colonne = colonne; }

    bool operator== (const Case src) const;

private:

    //Indice de la ligne de la case
    int m_ligne;
    //Indice de la colonne de la case
    int m_colonne;
};

#endif // CASE_H
